Vue.use(KalendarVue);
const { format } = dateFns;



let active = localStorage.getItem('activeUser');
console.log(active);
let startData = JSON.parse( localStorage.getItem(active) );
appointmentsFirst = startData;
if(appointmentsFirst == null){
  appointmentsFirst =[];
}


console.log(appointmentsFirst);

new Vue({
  el: "#vue_app",
  data: () => ({
    username: '',
    password: '',
    submitted: false,
    loading: false,
    returnUrl: '',
    error: '',
    firebaseUserLogin: false,
    firbaseUserName: '',
    toggle: false,
    statusSuccess: false,
    appointments:appointmentsFirst,
    calendar_settings: {
      style: "material_design",
      view_type: "Month",
      split_value: 20,
      cell_height: 10,
      scrollToNow: false,
      current_day: new Date() // Valid date
    },
    new_appointment: {
      description: null,
      title: null,
      place: null,
      attendees: null,
      Startfrom: null,
      time: null
    }
  }),
  mounted: function() {
    this.checkIfLogin();
  },
  methods: {
    checkIfLogin(){
      //Check if user exist or login
      firebase.auth().onAuthStateChanged(function(user) {
        window.user = user;
        if (window.user != null) {
          this.firebaseUserLogin = true;
        } else {
          this.firebaseUserLogin = false;
        }
      }.bind(this));
    },
    firebasesignIn(e){
       //New User Registration
       e.preventDefault();
       e.stopPropagation();
       this.statusSuccess = true;
       var emailr = document.querySelector('#email').value;
       var passwordr = document.querySelector('#password').value;
       // Register a new user
       firebase.auth().signInWithEmailAndPassword(emailr, passwordr)
       .then(function(successSignIn) {
          this.firebaseUserLogin = true;
          this.statusSuccess = false;
          this.firbaseUserName = emailr;
          localStorage.removeItem('activeUser');
          localStorage.setItem('activeUser' , emailr );
          let startData = JSON.parse( localStorage.getItem(emailr) );
          this.appointments =[];
          if(startData != null){
            this.appointments = startData;
          }

       }.bind(this))
       .catch(function (err) {
         this.statusSuccess = false;
       }.bind(this));
    },
    firebaseCreateUser(e){
        //New User Registration
        e.preventDefault();
        e.stopPropagation();
        this.statusSuccess = true;
        var emailr = document.querySelector('#email').value;
        var passwordr = document.querySelector('#password').value
        // Register a new user
        firebase.auth().createUserWithEmailAndPassword(emailr, passwordr)
        .then(function(successCreateUser) {
           this.toggle = false;
           this.statusSuccess = false;
           localStorage.removeItem('activeUser');
           localStorage.setItem('activeUser' , emailr );
           let startData = JSON.parse( localStorage.getItem(emailr) );
           this.appointments =[];
           if(startData != null){
             this.appointments = startData;
           }
        }.bind(this))
        .catch(function (err) {
          this.statusSuccess = false;
        }.bind(this));
    },
    firebaseLogout(e){
        //Logout user
        e.preventDefault();
        e.stopPropagation();
        firebase.auth().signOut();
        this.firebaseUserLogin = false;
    },



    completeAppointment(popup_data, form_data ) {
      let dateAppointment = format( popup_data.appointment_props.start_value.value, "YYYY-MM-DD" ),
          startTimeFilter = String(form_data.timestart).split(':')[0]+':00',
          endTimeFilter = String(form_data.timestart).split(':')[0]+':20',
          endTime = new Date(dateAppointment+" "+ endTimeFilter),
          startTime = new Date(dateAppointment+" "+startTimeFilter);
          payload = {
            data: {
              title: form_data.title,
              description: form_data.description,
              time: form_data.timestart,
              attendees: form_data.attendees,
              place: form_data.place,
              Startfrom: startTime
            },
            date: dateAppointment,
            from: startTime,
            to:   endTime
          };
          if( payload.from == 'Invalid Date' || payload.to == 'Invalid Date') {
            alert('Please select start and end Appointment time');
          }
      this.appointments.push(payload);
      this.new_appointment = {
        description: null,
        title: null,
        place: null,
        time: null,
        Startfrom: null,
        attendees: null
      };

      let activeUserNow = localStorage.getItem('activeUser');

      localStorage.setItem(activeUserNow , JSON.stringify(this.appointments) );
      popup_data.close_popup = true;
    },
    clearAppointment(id) {
      let allArray = this.appointments;
      for ( i=0; i<allArray.length; ++i) {
          if (allArray[i].from == id){
              allArray.splice(i, 1);
          }
      }
      let activeUserNow = localStorage.getItem('activeUser');
      // for ( i=0; i<this.appointments.length; ++i) {
      //     if (allArray[i].from == id){
      //         this.appointments.splice(i, 1);
      //     }
      // }

      localStorage.removeItem(activeUserNow);
      localStorage.setItem(activeUserNow,  JSON.stringify(this.appointments) );
    }

  }
});